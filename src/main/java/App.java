import com.github.theholywaffle.teamspeak3.TS3ApiAsync;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class App {

    public static TS3Config config = new TS3Config();
    public static TS3Query query;
    public static TS3ApiAsync api;
    public static int botId;

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args)  {

        String HOSTNAME = System.getenv("TS3BOT_HOSTNAME");
        Integer QUERYPORT = Integer.valueOf(System.getenv("TS3BOT_QUERYPORT"));
        String QUERYUSER = System.getenv("TS3BOT_QUERYUSER");
        String QUERYPASSWORD = System.getenv("TS3BOT_QUERYPASSWORD");
        Integer SERVERPORT = Integer.valueOf(System.getenv("TS3BOT_SERVERPORT"));
        String BOTNICKNAME = System.getenv("TS3BOT_NICKNAME");

        Integer AFKMOVETIME = Integer.valueOf(System.getenv("TS3BOT_AFKMOVETIME"));
        Integer AFKKICKTIME = Integer.valueOf(System.getenv("TS3BOT_AFKKICKTIME"));
        Integer AFKCHANNELID = Integer.valueOf(System.getenv("TS3BOT_AFKCHANNELID"));
        boolean AFKMOVE = Boolean.parseBoolean(System.getenv("TS3BOT_AFKMOVE"));
        boolean AFKKICK = Boolean.parseBoolean(System.getenv("TS3BOT_AFKKICK"));

        int movetime = AFKMOVETIME*60000;
        int kicktime = AFKKICKTIME*60000;
        int calcKickTime = movetime+kicktime;

        config.setHost(HOSTNAME);
        config.setQueryPort(QUERYPORT);
        config.setFloodRate(TS3Query.FloodRate.custom(1));
        config.setEnableCommunicationsLogging(false);

        query = new TS3Query(config);
        api = query.getAsyncApi();
        startup(AFKMOVE, AFKKICK);


        try {
            query.connect();
            logger.info("Connected", query.isConnected());
        } catch (Exception e){
            logger.error("Connection failed", e);
        }

        api.login(QUERYUSER, QUERYPASSWORD);
        api.selectVirtualServerByPort(SERVERPORT);
        if (!api.whoAmI().getUninterruptibly().getNickname().equals(BOTNICKNAME)){
            api.setNickname(BOTNICKNAME);
        }

        botId = api.whoAmI().getUninterruptibly().getId();

        if (AFKMOVE){
            Timer movetimer = new Timer();
            movetimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        moveAfk(movetime,AFKCHANNELID);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            },0,60000);
        }

        if (AFKKICK){
            Timer kicktimer = new Timer();
            kicktimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    kickAFK(calcKickTime, AFKCHANNELID);
                }
            },0,60000);
        }


    }

    private static void moveAfk(int movetime, int afkChannelId) throws InterruptedException {
        logger.info("Start looking for AFK Clients");
        List<Client> clients = api.getClients().getUninterruptibly();
        
        ArrayList<Client> clientArrayList = new ArrayList<>();

        ListIterator<Client> clientListIterator = clients.listIterator();
        while (clientListIterator.hasNext()){
            Client client = clientListIterator.next();
            if (client.getIdleTime() >= movetime
                    && client.getId() != botId
                    && client.getChannelId() != afkChannelId) {
                if (client.isOutputMuted() || client.isInputMuted()) {
                    api.moveClient(client.getId(), afkChannelId);
                    clientArrayList.add(client);
                    // api.editClient(client.getId(), ClientProperty.CLIENT_IDLE_TIME, String.valueOf(0));
                }
            }
        }


        logger.info("Finished moving AFK Clients - Clients moved: " + clientArrayList.size());
        for (Client client : clientArrayList) {
            logger.info(toString(client));
        }
    }

    private static void kickAFK(int kicktime, int afkChannelId){
        logger.info("Start kicking AFK Clients");
        List<Client> clients = api.getClients().getUninterruptibly();
        ListIterator<Client> clientListIterator = clients.listIterator();

        ArrayList<Client> clientArrayList = new ArrayList<>();

        while (clientListIterator.hasNext()){
            Client client = clientListIterator.next();
            //logger.info(String.valueOf(client.getChannelId()));
            if (client.getChannelId() == afkChannelId){
                if (client.getIdleTime() >= kicktime){
                    clientArrayList.add(client);
                }
            }
        }

        if (clientArrayList.size() != 0) {
            int[] kickArray = new int[clientArrayList.size()];
            for (int i = 0; i < clientArrayList.size(); i++) {
                kickArray[i]= clientArrayList.get(i).getId();
            }
            api.kickClientFromServer("Kicked due to too long absence", kickArray).getUninterruptibly();
        }

        logger.info("Finished kicking AFK Clients - Clients kicked: " + clientArrayList.size());
        for (Client client : clientArrayList) {
            logger.info(toString(client));
        }

    }


    public static String toString(Client client){
        return " --> " + client.getNickname() + " - " + client.getIdleTime()/60000 + "min";
    }

    private static void startup(boolean move, boolean kick){
        logger.info("TS3 AFK Bot - by Felix Hövel");
        logger.info("AFK Move = " + move);
        logger.info("AFK Kick = " + kick);
    }

}
